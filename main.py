#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Receives HTTP POST requests and sends content as message to Telegram.
Environment variables:
API_TOKEN - Telegram Bot API token
CHAT_ID - ID of chat in Telegram - will be used as target
PASSWORD - Incoming requests authorisation password
PORT - Port for web server (default: 8080)
DEBUG - If true - prints debug messages
"""
import logging
try:
    import simplejson as json
except ImportError:
    import json
from os import getenv
from http.server import BaseHTTPRequestHandler, HTTPServer
import telegram


class Configuration:
    class TelegramConfiguration:
        def __init__(self, token, chat_id):
            self.token = token
            self.chat_id = chat_id

    class ServerConfiguration:
        def __init__(self, ip, port, password, debug):
            self.ip = ip
            self.port = port
            self.password = password
            self.auth_enabled = password is not None
            self.debug = debug

    def __init__(self):
        self.telegram = self.TelegramConfiguration(
            token=getenv('API_TOKEN', None),
            chat_id=getenv('CHAT_ID', None)
        )
        self.server = self.ServerConfiguration(
            ip='0.0.0.0',
            port=getenv('PORT', 8080),
            password=getenv('PASSWORD', None),
            debug=getenv('DEBUG', 'false').lower() == 'true',
        )
        if not self.telegram.token or not self.telegram.chat_id:
            logging.error('You have not passed API_TOKEN and\or CHAT_ID')


class Telegram:
    def __init__(self):
        self.config = Configuration().telegram
        self.bot = telegram.Bot(token=self.config.token)

    def send_message(self, title: str, text: str):
        if not title and not text:
            logging.error('Both text and title are empty')
            return
        if title:
            text = f"*{title}*\n{text}"
        self.bot.send_message(chat_id=self.config.chat_id, parse_mode='Markdown', text=text)
        pass


class Server(BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        super().__init__(request, client_address, server)

    def do_POST(self):
        self.new_request()
        pass

    def do_GET(self):
        # Send response status code
        self.send_response(400)
        # Send headers
        self.send_header('Content-type', 'javascript/json')
        self.end_headers()
        response = {
            'status': 'error',
            'cause': 'only POST requests are accepted'
        }
        self.wfile.write(bytes(json.dumps(response), "utf8"))
        pass

    def new_request(self):
        config = Configuration().server
        telegram = Telegram()
        try:
            raw = self.rfile.read(int(self.headers['Content-Length']))
            data = json.loads(raw)
            if config.auth_enabled and 'password' not in data:
                logging.warning(f"Received request without password from {self.client_address}")
                logging.debug(f"JSON:\n{json.dumps(data, indent=2, sort_keys=True)}")
                self.send_response(401)
                self.end_headers()
                return
            elif config.auth_enabled and data['password'] != config.password:
                logging.warning(f"Wrong password from {self.client_address}")
                logging.debug(f"JSON:\n{json.dumps(data, indent=2, sort_keys=True)}")
                self.send_response(401)
                self.end_headers()
                return
        except Exception as e:
            logging.error(f"Incoming request parsing error: {str(e)}")
            logging.debug(f"Raw package content: {raw}")
            self.send_response(500)
            self.end_headers()
            return
        # answer on request
        try:
            telegram.send_message(
                title=data.get('title'),
                text=data.get('text')
            )
            # Send response status code
            self.send_response(200)
            self.end_headers()
        except Exception as e:
            logging.error(f'Request error: {str(e)}')


def main():
    """
    Simply starts web server.
    :return:
    """
    logging.info('Server started.')
    config = Configuration().server
    server_address = (config.ip, int(config.port))
    httpd = HTTPServer(server_address, Server)
    logging.info('Listening requests on {0}:{1}'.format(config.ip, config.port))
    httpd.serve_forever()
    pass


if __name__ == '__main__':
    logging_format = '%(asctime)s [%(levelname)s] Line: %(lineno)d > %(message)s'
    if Configuration().server.debug:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.INFO
    logging.basicConfig(format=logging_format, level=logging_level)
    try:
        main()
    except Exception as e:
        logging.error(f'Main loop exception: {str(e)}')
