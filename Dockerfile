FROM python:3-alpine
WORKDIR /app
COPY requirements.txt .
RUN apk add --no-cache --virtual .build-deps g++ python3-dev libffi-dev openssl-dev && \
    pip install --upgrade pip setuptools && \
    pip install --upgrade -r requirements.txt && \
    apk del .build-deps
COPY main.py .
CMD python main.py