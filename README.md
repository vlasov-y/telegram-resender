# Brief
Server on Python3 receives *POST* requests with *JSON* inside, and sends messages to *Telegram* chat.

# Environment variables
| NAME | REQUIRED | DESCRIPTION | DEFAULT |
| ---- | -------- | ----------- | ------- |
| API_TOKEN | yes | Telegram bot token from @BotFather | None |
| CHAT_ID | yes | ID of conversation to use | None |
| PASSWORD | no | See description below | None |
| PORT | no | Web server listening port | 8080 |
| DEBUG | no | Additional verbosity | False |

# JSON description
```json
{
  "title": "Text of the title",
  "text": "Text of main body",
  "password": "Password"
}
```
`.title` and `.text` supports Markdown.
*Notice*: text in `.title` is already between `**`

| KEY | REQUIRED | DESCRIPTION | 
| ---- | -------- | ----------- |
| `.text` | yes | Message body text |
| `.title` | no | Will be placed in first line with bold text |
| `.password` | if PASSWORD is set | Password for authentication |

If you did not set `PASSWORD` environment variable, then `.password` is not needed and will be ignored.
If you have setup `PASSWORD` environment variable, then `.password` is required.
If you use HTTP then using of PASSWORD is useless. In case of HTTPS it is useful.

# Examples
```bash
# PASSWORD is not set
curl -X POST -d '{"text":"`Code` - *bold* - _italic_"}' localhost:8080
# with title
curl -X POST -d '{"title": "This is title!", "text":"`Code` - *bold* - _italic_"}' localhost:8080
# with PASSWORD set
curl -X POST -d '{"password": "qwer1234", "text":"`Code` - *bold* - _italic_"}' localhost:8080
```

# Getting API_TOKEN and CHAT_ID
1. Write to **@BotFather** and create new bot - there you will find `API_TOKEN` key.
2. Write to your bot from personal conversation or from group.
3. Send written message to **@userinfobot** - there you will find `CHAT_ID` key.  
It is possible that you will need to add '-' in front of group ID.  
Example: group_id = '12345678' -> set CHAT_ID to '-12345678'